import 'dart:io';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:new_location/map_ui.dart';
import 'package:rxdart/rxdart.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_saver/image_saver.dart';
import 'dart:convert';
import 'package:path_provider/path_provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: "Assig 2"),
    );
  }
}


class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}
class _MyHomePageState extends State<MyHomePage> {
  var location;
  File _image;
  var counter = 0;

  Future<String> get _localPath async {
    final directory = await getExternalStorageDirectory();
    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File("$path/imageData_${counter}.json");
  }

  Future<File> writeJson(String jsonData) async {
    counter++;
    final file = await _localFile;

    // Write the file
    return file.writeAsString(jsonData);
  }

  Future<String> readJson() async {
    try {
      final file = await _localFile;

      // Read the file
      String contents = await file.readAsString();

      return contents;
    } catch (e) {
      // If encountering an error, return 0
      return "NO DATA";
    }
  }

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    File savedFile = await ImageSaver.toFile(fileData: image.readAsBytesSync());
    if (!mounted) {
      return;
    }

    setState(() {
      _image = savedFile;
      var base64 = Base64Encoder().convert(_image.readAsBytesSync());
      print(base64);

      var toSerialise = { "image_data": "IMAGE_DATA for image number:${counter + 1}", "geo_location": { "lat": location.latitude, "lng":  location.longitude}, "user_notes": "Hello, World!" };

      var jsonData = JsonEncoder().convert(toSerialise);
      print(jsonData);

      Observable.fromFuture(writeJson(jsonData)).listen((onData){
        print("JSON written successfully!");

        Observable.fromFuture(readJson()).listen((onData){
          print(onData);
        });

      }, onError: print);

    });
  }

  _MyHomePageState(){

    Observable.fromFuture(Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high))
        .listen((postion) {
          setState(() {
            location = postion;
          });
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Container(child: location == null ? CircularProgressIndicator() : MapUiPage(location)),
      ),
      floatingActionButton:  FloatingActionButton(
        child: const Icon(Icons.photo_camera),
        onPressed: getImage,
        tooltip: 'Pick Image',
//        Imagchild: new Icon(Icons.add_a_photo),
   )
    );
  }
}
